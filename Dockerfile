FROM golang:buster

WORKDIR /app

RUN go install gitlab.com/greizgh/bookshelf@latest

RUN mkdir -p /books

# COPY refresh.sh /usr/local/bin/refresh.sh
# RUN chmod +x /usr/local/bin/refresh.sh

# RUN touch refresh
# RUN echo '#!/bin/sh' >> refresh
# RUN echo '' >> refresh
# RUN echo 'bookshelf index /books' >> refresh
# RUN mv refresh /etc/periodic/15min/refresh
# RUN chmod +x /etc/periodic/15min/refresh

# RUN echo '*       *       *       *       *       /usr/local/bin/refresh.sh' >> /etc/crontabs/root
# RUN echo ' */5  *  *  *  * /usr/local/bin/reminders.sh' >> /etc/crontabs/root

# Run crond  -f for Foreground
# RUN /usr/sbin/crond -bS
# CMD ["/usr/sbin/crond", "-f"]

EXPOSE 8080

CMD [ "bookshelf", "serve", "-b", "0.0.0.0", "-p", "8080" ]
