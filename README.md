# bookshelf

Docker image for https://gitlab.com/greizgh/bookshelf a lightweight epub online catalog.

```bash
docker run -d --name=bookshelf \
  -p 8786:8080 \
  -v /path/to/books:/books \
  --restart unless-stopped \
  ewilanriviere/bookshelf
docker exec -it bookshelf bookshelf index /books
```

bookshelf is available at http://localhost:8786

> Books index will be refreshed every 15 minutes.

## Build

```bash
docker build -t ewilanriviere/bookshelf .
```

## Bash

```bash
docker container exec -it bookshelf /bin/sh
```

## Docker hub

```bash
docker push ewilanriviere/bookshelf
```

## Examples

### Local

```bash
docker run -d --name=bookshelf \
  -p 8687:8080 \
  -v /Users/ewilan/Documents/books/linked/novel:/books \
  --restart unless-stopped \
  ewilanriviere/bookshelf
docker exec -it bookshelf bookshelf index /books
```

### Synology NAS

```bash
docker run -d --name=bookshelf \
  -p 8687:5000 \
  -e PUID=0 \
  -e PGID=0 \
  -v /volume1/books/books:/books \
  --restart always \
  ewilanriviere/bookshelf
docker exec -it bookshelf bookshelf index /books
```
